import * as THREE from 'three'
import gsap from 'gsap'

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Object
const geometry = new THREE.BoxGeometry(1, 1, 1)
const material = new THREE.MeshBasicMaterial({ color: 0xff0000 })
const mesh = new THREE.Mesh(geometry, material)
scene.add(mesh)

// Sizes
const sizes = {
    width: 800,
    height: 600
}

// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height)
camera.position.z = 3
scene.add(camera)

// Renderer
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.render(scene, camera)

//cách để cân bằng Frame rate giữa các computer khi xử lý animation
/*
    Do các tốc độ xử lý của các máy tính khác nhau nên nếu ta render theo frame thì sẽ có sự trên lệch về tốc dộ render của khung hình
        ex: may Khải Hoàn 60FPS nhưng máy MD thì lại 120FPS
        => máy MD animation sẽ run nhanh hơn x2 lần so với máy Khải Hoàn.
    
    Có nhiều cách xử lý vấn đề này;
    C1: Tính delta giữa hàm Date.now() và Date.now() khi run code render Frame. Lấy delta nhân cho cơ số thay đổi thì tốc độ sẽ như nhau.
        Đây là cách đưa về tốc độ thay đổi của khung hình.
        ex: khi quay mesh, delta time của máy Hoàn là 10 máy MD là 5 (máy xử lý càng nhanh thì delta sẽ càng nhỏ).
        thì 1 frame của Khải Hoàn mesh phải quay cơ số x * 10. còn máy MD sẽ là x * 5
        => animation cân bằng giữa 2 máy.
        Code: 
        let time = Date.now();

        function perFrameRender() {
            //update mesh
            // mesh.rotation.y += 0.01;

            const current = Date.now();
            const delta = current - time;
            time = current;

            mesh.rotation.y += 0.0015 * delta;
            // mesh.position.x += 0.01;
            // camera.lookAt(mesh.position);

            renderer.render(scene, camera);

            window.requestAnimationFrame(perFrameRender)
        }

    C2: Dùng class Three.Clock của ThreeJS. Là một build-in trong ThreeJS giúp tính thời gian sau khi được initial.
    CODE:
    const clock = new THREE.Clock();

    function perFrameRender() {
        //update mesh

        const elapsedTime = clock.getElapsedTime();

        // mesh.position.y = Math.sin(elapsedTime);
        // mesh.position.x = Math.cos(elapsedTime);
        // camera.lookAt(mesh.position)

        renderer.render(scene, camera);

        window.requestAnimationFrame(perFrameRender)
    }



*/

gsap.to(mesh.position, { duration: 1, delay: 1, x: 2 })
gsap.to(mesh.position, { duration: 1, delay: 2, x: 0 })

const clock = new THREE.Clock();

function perFrameRender() {
    //update mesh

    const elapsedTime = clock.getElapsedTime();

    // mesh.position.y = Math.sin(elapsedTime);
    // mesh.position.x = Math.cos(elapsedTime);
    // camera.lookAt(mesh.position)

    renderer.render(scene, camera);

    window.requestAnimationFrame(perFrameRender)
}

perFrameRender()
